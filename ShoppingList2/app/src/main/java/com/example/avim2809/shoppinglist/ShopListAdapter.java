package com.example.avim2809.shoppinglist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class ShopListAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener,View.OnClickListener,Serializable {
    final int CAMEREA_REQUEST = 1;


    public List<Product> getProductList() {
        return ProductList;
    }

    public void setProductList(List<Product> productList) {
        ProductList = productList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private List<Product> ProductList;
    private Context context;
    private Activity activity;


    ShopListAdapter(List<Product> productList, Context context) {
        ProductList = productList;
        this.context = context;
        Activity activity = (Activity)context;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return ProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return ProductList.get(position);
    }

    @Override
    public long getItemId(int position)  {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // inflate new line cell layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            convertView = layoutInflater.inflate(R.layout.shop_list_line_layout,parent,false);
        }

        Product product = ProductList.get(position);
        CheckBox checkBox = convertView.findViewById(R.id.purchased_cb);
        EditText amount_tv = convertView.findViewById(R.id.prod_amount_et);
        TextView prod_name_tv = convertView.findViewById(R.id.prod_name_tv);
        Button photo_btn = convertView.findViewById(R.id.takephoto_btn);
        ImageView imageView = convertView.findViewById(R.id.img_view);


        // fill data in Layout elements from product object
        checkBox.setChecked(product.isChecked());
        checkBox.setTag(position);
        checkBox.setOnCheckedChangeListener(this);
        photo_btn.setTag(position);
        photo_btn.setOnClickListener(this);
        amount_tv.setText(product.getAmount()+"");
        prod_name_tv.setText(product.getName());
        if (product.getPhoto() != null)
        {
            imageView.setImageBitmap(product.getPhoto());
        }

        return convertView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //TODO: implement what to with text if its checked
        int index = (Integer) buttonView.getTag();
        Product product = ProductList.get(index);
        product.setChecked(isChecked);

    }

    @Override
    public void onClick(View v) {
        Button button = (Button)v;
        int index = (Integer) button.getTag();
        Product product = ProductList.get(index);
        Intent cam_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cam_intent.putExtra("index",index);
        this.activity.startActivityForResult(cam_intent,CAMEREA_REQUEST); // intent action result would be handled in activity





    }
}
