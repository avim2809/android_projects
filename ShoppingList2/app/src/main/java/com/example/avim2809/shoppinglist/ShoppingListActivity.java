package com.example.avim2809.shoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;


public class ShoppingListActivity extends AppCompatActivity {
    final int CAMEREA_REQUEST = 1;
    private String username;
    private String password;
    private TextView title_textview;
    private ArrayList<Product> products = null;
    private ShopListAdapter shopListAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_list_layout);
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");
        title_textview = findViewById(R.id.shop_list_tv);

        ListView listView = findViewById(R.id.shop_list_view);
        if (products == null)  products = new ArrayList<>();
        if (shopListAdapter == null) shopListAdapter = new ShopListAdapter(products,this);
        listView.setAdapter(shopListAdapter);
        load_products();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMEREA_REQUEST && resultCode == RESULT_OK)
        {
            // handle camera picture save
            int index = data.getIntExtra("index",0);
            Product product = products.get(index);
            Bitmap bitmap = (Bitmap)data.getExtras().get("data");
            product.setPhoto(bitmap);
            product.setBitmap_string(product.BitMapToString(bitmap));
            shopListAdapter.setProductList(products);
            shopListAdapter.notifyDataSetChanged();
        }
    }


    private void save_products()
    {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("username",username);
        prefsEditor.putString("password",password);
        prefsEditor.commit();
        //save the products
        if (products!= null && products.isEmpty() == false)
        {

            try {
                File outputFile = new File(getFilesDir(), username);
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outputFile));
                oos.writeObject(products);
                oos.flush();
                oos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        save_products();

    }

    void load_products()
    {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        // load products for this user
        String origin_key = "origin";
        String username = null;
        boolean has_products = false;


        if (getIntent().hasExtra(origin_key))
        {
            String origin = getIntent().getStringExtra(origin_key);
            if (origin.equals("ProductListActivity"))
            {
                // take products from intent
                username = getIntent().getStringExtra("username");
                if (getIntent().hasExtra("has_products"))
                {
                    has_products = getIntent().getBooleanExtra("has_products",false);

                }
            }
        }

        else if(appSharedPrefs.contains("username"))
        {
            //take products from persistent storage
            username = appSharedPrefs.getString("username","");
            has_products = true;

        }
        if (username != null && username.isEmpty() == false && has_products)
        {
            try
            {
                File inputfile = new File(getFilesDir(), username);
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(inputfile));
                products = (ArrayList<Product>)ois.readObject();
                ois.close();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            for (Product product:products)
            {
                if (product.getBitmap_string() !=null)
                {
                    product.setPhoto(product.StringToBitMap(product.getBitmap_string()));
                }
            }
            shopListAdapter.setProductList(products);
            shopListAdapter.notifyDataSetChanged();

        }

    }
}
