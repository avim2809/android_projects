package com.example.avim2809.shoppinglist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;


public class Product implements Serializable {
    private static int counter = 0;
    private String name;
    private String Category;
    private int amount;
    transient private Bitmap photo;
    private boolean checked;
    private int product_id;
    private String bitmap_string = null;

    public String getBitmap_string() {
        return bitmap_string;
    }

    public void setBitmap_string(String bitmap_string) {
        this.bitmap_string = bitmap_string;
    }

    public Product() {
        this.checked = false;
    }

    public boolean isChecked() {

        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Bitmap getPhoto() {

        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }


    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Product.counter = counter;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Product(String name, int amount, String category) {
        this.name = name;
        this.amount = amount;
        Category = category;
        this.photo = null;
        this.checked = false;
        this.product_id = Product.counter;
        Product.counter++;



    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setCategory(String category) {
        Category = category;
    }


    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public String getCategory() {
        return Category;
    }

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        if (photo != null) photo.compress(Bitmap.CompressFormat.JPEG,50,out);

        out.defaultWriteObject();

    }
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.photo = BitmapFactory.decodeStream(in);
        in.defaultReadObject();
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

}
