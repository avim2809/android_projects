package com.example.avim2809.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;

public class NavigateActivity extends Activity implements CompoundButton.OnClickListener{
    String username;
    String password;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_navigator);
        Button prod_list_btn = findViewById(R.id.navigator_prod_list_btn);
        prod_list_btn.setOnClickListener(this);
        Button shop_list_btn = findViewById(R.id.navigator_shop_list_btn);
        shop_list_btn.setOnClickListener(this);
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");



    }

    @Override
    public void onClick(View v) {
        Button button = (Button)v;
        int id = v.getId();

        switch (id)
        {
            case R.id.navigator_prod_list_btn:
                // open the product list
                Intent intent = new Intent(this,ProductListActivity.class);
                intent.putExtra("username",username);
                intent.putExtra("password",password);
                startActivity(intent);
                break;

            case R.id.navigator_shop_list_btn:
                // open the shopping list
                Intent intent2 = new Intent(this,ShoppingListActivity.class);
                intent2.putExtra("username",username);
                intent2.putExtra("password",password);
                startActivity(intent2);
                break;


        }
    }
}
