package com.example.avim2809.shoppinglist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ProductListAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener,View.OnClickListener{
    private List<Product> ProductList;
    private Context context;

    public List<Product> getProductList() {
        return ProductList;
    }

    public void setProductList(List<Product> productList) {
        ProductList = productList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ProductListAdapter(List<Product> productList, Context context) {
        ProductList = productList;
        this.context = context;

    }

    @Override
    public int getCount() {
        return ProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return ProductList.get(position);
    }

    @Override
    public long getItemId(int position)  {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // inflate new line cell layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            convertView = layoutInflater.inflate(R.layout.prod_list_line_layout,parent,false);
        }

        Product product = ProductList.get(position);
        CheckBox checkBox = convertView.findViewById(R.id.choose_cb);
        EditText amount_tv = convertView.findViewById(R.id.prod_amount_prod_list_et);
        TextView prod_name_tv = convertView.findViewById(R.id.prod_name_tv_prod_list);
        TextView category_tv = convertView.findViewById(R.id.prod_category_tv_prod_list);
        Button photo_btn = convertView.findViewById(R.id.takephoto_btn_prod_list);
        ImageView imageView = convertView.findViewById(R.id.img_view_prod_list);


        // fill data in Layout elements from product object
        checkBox.setChecked(product.isChecked());
        checkBox.setTag(position);
        checkBox.setOnCheckedChangeListener(this);
        photo_btn.setTag(position);
        photo_btn.setOnClickListener(this);
        amount_tv.setText(product.getAmount()+"");
        prod_name_tv.setText(product.getName());
        if (product.getPhoto() != null)
        {
            imageView.setImageBitmap(product.getPhoto());
        }

        return convertView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //TODO: implement what to with text if its checked
        int index = (Integer) buttonView.getTag();
        Product product = ProductList.get(index);
        product.setChecked(isChecked);

    }

    @Override
    public void onClick(View v) {
        Button button = (Button)v;
        int index = (Integer) button.getTag();
        Product product = ProductList.get(index);
        //TODO: implement INTENT Camera action

    }
}
