package com.example.avim2809.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        final EditText username_et = findViewById(R.id.username_et);
        final EditText password_et = findViewById(R.id.password_et);
        Button login_btn = findViewById(R.id.login_btn);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = username_et.getText().toString();
                String password = password_et.getText().toString();
                Intent intent = new Intent(MainActivity.this,NavigateActivity.class);
                intent.putExtra("username",username);
                intent.putExtra("password",password);
                startActivity(intent);


            }
        });




    }
}
