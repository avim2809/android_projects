package com.example.avim2809.shoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity implements View.OnClickListener,Serializable{
    private String username;
    private String password;
    private ArrayList<Product> products = null;
    private TextView title_textview;
    private EditText prod_name_et,prot_cat_et;
    private ListView listView;
    private ProductListAdapter productListAdapter = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list_layout);
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");
        title_textview = findViewById(R.id.product_list_tv);
        prod_name_et = findViewById(R.id.prod_name_et);
        prot_cat_et = findViewById(R.id.prod_category_tv);
        Button add_btn = findViewById(R.id.add_prod_btn);
        Button create_shop_list_btn = findViewById(R.id.create_shop_list_btn);
        add_btn.setOnClickListener(this);
        create_shop_list_btn.setOnClickListener(this);
        listView = findViewById(R.id.product_list_view);
        if (products == null)  products = new ArrayList<>();
        if (productListAdapter == null)  productListAdapter = new ProductListAdapter(products,this);
        listView.setAdapter(productListAdapter);
        load_products();





    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id)
        {
            case R.id.add_prod_btn:
                // add product
                Product product = new Product();
                String prod_name = prod_name_et.getText().toString();
                String prod_category = prot_cat_et.getText().toString();
                product.setName(prod_name);
                product.setCategory(prod_category);
                products.add(product);
                //update the list view
                productListAdapter.setProductList(products);
                productListAdapter.notifyDataSetChanged();
                break;
            case R.id.create_shop_list_btn:
                // create shopping list and open shopping list activity
                Intent shop_list_intent = new Intent(this,ShoppingListActivity.class);
                shop_list_intent.putExtra("username",username);
                shop_list_intent.putExtra("password",password);
                shop_list_intent.putExtra("origin","ProductListActivity");
                //serialize the product_list
                //save the products
                if (products!= null) shop_list_intent.putExtra("has_products", true);
                else shop_list_intent.putExtra("has_products", false);
                save_products();
                startActivity(shop_list_intent);
                break;


        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        save_products();


    }

    private void save_products()
    {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("username",username);
        prefsEditor.putString("password",password);
        prefsEditor.commit();
        //save the products
        if (products!= null&& products.isEmpty() == false)
        {

            try {
                File outputFile = new File(getFilesDir(), username);
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outputFile));
                oos.writeObject(products);
                oos.flush();
                oos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    private void load_products()
    {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        // load products for this user
        if(appSharedPrefs.contains("username"))
        {
            username = appSharedPrefs.getString("username","");
            try
            {
                File inputfile = new File(getFilesDir(), username);
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(inputfile));
                products = (ArrayList<Product>)ois.readObject();
                ois.close();


            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            productListAdapter.setProductList(products);
            productListAdapter.notifyDataSetChanged();

        }


    }
}
